package com.example.neno.chatapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Active extends ActionBarActivity {
    private AsyncHttpClient httpClient = new AsyncHttpClient();
    private Gson mGson = new Gson();
    private ListView listView;
    private AdapterActive adapter;

    JSONParser jsonParser = new JSONParser();
    private ProgressDialog pDialog;
    private String url = "http://nenotst.esy.es/viewpoll.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active);
        listView = (ListView) findViewById(R.id.listViewActive);

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM. HH:mm");
        String currentDateandTime = sdf.format(new Date());
        Log.d("vrijeme",currentDateandTime);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                                Intent intent = new Intent(view.getContext(), ActiveDetails.class);
                                                intent.putExtra("pozicija", String.valueOf(i));
                                                Log.d("pozicija",i+"");
                                                startActivity(intent);
                                            }
        });


        httpClient.get(url, new JsonHttpResponseHandler() { //get zahtjev,dobavi JSON sa URL

            @Override
            public void onSuccess(JSONArray response) {

                Log.d("pre_RESPONSE", response.toString());

                DataStorage.ankete = mGson.fromJson(response.toString(), Anketa[].class);
                adapter = new AdapterActive(getApplicationContext());//adapter u trenutnom aktivitiju
                listView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Throwable error) {
                Toast.makeText(getApplicationContext(), "Error: ne može se pristupiti serveru.", Toast.LENGTH_LONG).show();
            }


        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_active, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
