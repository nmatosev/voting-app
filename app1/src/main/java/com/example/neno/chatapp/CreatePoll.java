package com.example.neno.chatapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class CreatePoll extends ActionBarActivity {

    LinearLayout containerLayout;
    static int totalEditTexts = 0;
    Button dodaj_opciju;
    Button potvrdi;
    Context context;
    EditText postaviPitanje;
    private ProgressDialog pDialog;
    public final static String DEFAULT = "N/A";
    JSONParser jsonParser = new JSONParser();
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PITANJE = "pitanje";
    private static final String TAG_OPCIJE = "opcija";
    private static final String TAG_DATUM = "datum";
    private static final String TAG_AUTOR = "autor";
    //private static final String TAG ITEMI
    private static final String TAG_MESSAGE = "message";
    private static final String url = "http://nenotst.esy.es/createpoll.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_poll);

        containerLayout = (LinearLayout) findViewById(R.id.ll);
        dodaj_opciju = (Button) findViewById(R.id.btn_add_new_option);
        potvrdi = (Button) findViewById(R.id.btn_potvrdi);
        postaviPitanje = (EditText) findViewById(R.id.et_new_pool);

        final List<EditText> allEds = new ArrayList<EditText>();

        dodaj_opciju.setOnClickListener(new View.OnClickListener() {

            EditText editText;
            int id;

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                //for(totalEditTexts=0;totalEditTexts<15;totalEditTexts++) {
                totalEditTexts++;
            /*    if (totalEditTexts > 8)
                    //return;
                {
                    Toast.makeText(getApplicationContext(), "Nije moguće unijeti više od 8 opcija!", Toast.LENGTH_SHORT).show();
                    allEds.clear();
                    totalEditTexts = 0;
                    return;////skače na kraj funkcije
                }*/

                //  EditText editText = new EditText(getBaseContext());
                editText = new EditText(CreatePoll.this);
                allEds.add(editText);
               // Log.d("lista", allEds.toString());
                editText.setId(id);
                id++;
                Log.d("id", id + "");

                if (id > 8)
                //return;
                {
                    Toast.makeText(getApplicationContext(), "Nije moguće unijeti više od 8 opcija!", Toast.LENGTH_SHORT).show();
                    allEds.clear();
                    totalEditTexts = 0;
                    containerLayout.removeAllViews();
                    id=0;

                    return;////skače na kraj funkcije
                }


                containerLayout.addView(editText);
                editText.setGravity(Gravity.RIGHT);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) editText.getLayoutParams();

                layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
                layoutParams.setMargins(23, 34, 0, 0);
                // RelativeLayout.LayoutParams()

                editText.setLayoutParams(layoutParams);
                //if you want to identify the created editTexts, set a tag, like below
                editText.setTag("et_option" + totalEditTexts);


            }


        });


        potvrdi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] items = new String[allEds.size()];
                String pitanje = postaviPitanje.getText().toString();

                if (pitanje != null && !pitanje.isEmpty()) {
                    Log.d("listaaa", allEds.toString());

                    SharedPreferences sharedPreferences = getSharedPreferences("upitnik", 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("size", items.length);


////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////IZ SVIH EDITTEXTOVA UZMI STRING I STAVI GA U SHARED PREF////

                    for(int i=0;i<items.length;i++) {
                        items[i] = i + 1 + ") " + allEds.get(i).getText().toString();
                        Log.d("alleds",allEds.get(i).getText().toString() );
                        editor.putString("items"+i, items[i]);
                        Log.d("elementi",items[i]);


                    }

                    editor.putString("pitanje",pitanje);
                    editor.commit();

                    new Salji_u_Bazu().execute();
                }
            }


        });
    }


    ////////////////////////////////////////////////////////////
    ///////////////////PARAMS,PROGRESS,RESULT/////////////////
    ////////////////////////////////////////////////////////////
    class Salji_u_Bazu extends AsyncTask<String[], String, String> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CreatePoll.this);
            pDialog.setMessage("Pristup bazi podataka ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String[]... args) {


            SharedPreferences sharedPreferences = getSharedPreferences("upitnik", 0);

            String pitanje = sharedPreferences.getString("pitanje",DEFAULT);
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM. HH:mm");
            String currentDateandTime = sdf.format(new Date());

            SharedPreferences sharedPreferences1 = getSharedPreferences("login_podaci", Context.MODE_PRIVATE);
            String name = sharedPreferences1.getString("name",DEFAULT);

            Log.d("vrijeme",currentDateandTime);

            //String opcije = sharedPreferences.getString("opcije",DEFAULT);
            //Log.d("pitanje i opcije",pitanje+opcije);

            int size = sharedPreferences.getInt("size", 0);//dobavi veličinu niza
            Log.d("SIZE",size+"");

            String array[] = new String[size];   //deklariraj array s tom veličinom i iteriraj

            for(int i=0;i<size;i++) {
                array[i] = sharedPreferences.getString("items"+i, DEFAULT);

            }

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair(TAG_PITANJE,pitanje));
            params.add(new BasicNameValuePair(TAG_DATUM,currentDateandTime));
            params.add(new BasicNameValuePair(TAG_AUTOR,name));
            //params.add(new BasicNameValuePair(TAG_OPCIJE+i,));
            for(int i=0;i<size;i++){
                //String itemi[] = new String[i];
                params.add(new BasicNameValuePair(TAG_OPCIJE+i,array[i]));
                Log.d("forpetlja", array[i]);

            }



            JSONObject json = jsonParser.makeHttpRequest(url,
                    "POST", params);

            // check json success tag
            try {
                Log.d("try", "Pokušaj pristupa bazi");
                int success = json.getInt(TAG_SUCCESS);
                Log.d("resp", json.getString(TAG_SUCCESS));


                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    Log.d("response", "uspjeh POST-A");
                    finish();
                } else {
                    Log.d("GRESKA", "greska");
                }



            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("Debug", e.toString());

            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            Intent intent = new Intent(getApplicationContext(), Izbornik.class);
            startActivity(intent);
            pDialog.dismiss();

        }




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_poll, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
