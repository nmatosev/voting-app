package com.example.neno.chatapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ActiveDetails extends ActionBarActivity {
    RadioButton rda, rdb, rdc, rdd, rde, rdf, rdg, rdh;
    Button butSave;
    TextView rez1, rez2, rez3, rez4, rez5, rez6, rez7, rez8;
    private ProgressDialog pDialog;
    public final static String DEFAULT = "N/A";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PITANJE = "pitanje";
    private static final String TAG_OPCIJE = "opcija";
    private static final String TAG_DATUM = "datum";
    private static final String TAG_AUTOR = "autor";
    private static final String TAG_ODABIR = "odabir";
    private static final String TAG_VOTER = "voter";
    private static final String TAG_GLASAO = "glasao";
    private AsyncHttpClient httpClient = new AsyncHttpClient();
    private Gson mGson = new Gson();
    private View mChart;
    private CategorySeries mSeries = new CategorySeries("");
    /**
     * The main renderer for the main dataset.
     */
    private DefaultRenderer mRenderer = new DefaultRenderer();
    private static int[] COLORS = new int[]{Color.GREEN, Color.BLUE, Color.MAGENTA, Color.CYAN};
    private String[] mMonth = new String[]{
            "1", "2", "3", "4", "5", "6",
            "7", "7", "9", "10", "11", "12"
    };


    JSONParser jsonParser = new JSONParser();

    private static final String TAG_MESSAGE = "message";
    private static final String url = "http://nenotst.esy.es/savepoll.php";
    private static final String url_rez = "http://nenotst.esy.es/rezultati.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_details);
        final TextView pitanje = (TextView) findViewById(R.id.pitanje);
        rda = (RadioButton) findViewById(R.id.radioButton);
        rdb = (RadioButton) findViewById(R.id.radioButton2);
        rdc = (RadioButton) findViewById(R.id.radioButton3);
        rdd = (RadioButton) findViewById(R.id.radioButton4);
        rde = (RadioButton) findViewById(R.id.radioButton5);
        rdf = (RadioButton) findViewById(R.id.radioButton6);
        rdg = (RadioButton) findViewById(R.id.radioButton7);
        rdh = (RadioButton) findViewById(R.id.radioButton8);

        rez1 = (TextView) findViewById(R.id.tv_opcija1);
        rez2 = (TextView) findViewById(R.id.tv_opcija2);
        rez3 = (TextView) findViewById(R.id.tv_opcija3);
        rez4 = (TextView) findViewById(R.id.tv_opcija4);
        rez5 = (TextView) findViewById(R.id.tv_opcija5);
        rez6 = (TextView) findViewById(R.id.tv_opcija6);
        rez7 = (TextView) findViewById(R.id.tv_opcija7);
        rez8 = (TextView) findViewById(R.id.tv_opcija8);
        butSave = (Button) findViewById(R.id.btnSave);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("pozicija");
            Integer position = Integer.valueOf(value);//kastaj extra iz maina u int

            String rezultati = (DataStorage.ankete[position].rezultat).toString();

            SharedPreferences sharedPreferences1 = getSharedPreferences("odabir", 0);

            //////////////////////////////////////////////////////////////////////////////////
            ///////////////BROJANJE GLASOVA//////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////
            Log.d("rezultati", rezultati);
            int count1 = rezultati.length() - rezultati.replace("1", "").length();
            Log.d("count1e", count1 + "-" + rezultati.length());
            int count2 = rezultati.length() - rezultati.replace("2", "").length();
            int count3 = rezultati.length() - rezultati.replace("3", "").length();
            int count4 = rezultati.length() - rezultati.replace("4", "").length();
            int count5 = rezultati.length() - rezultati.replace("5", "").length();
            int count6 = rezultati.length() - rezultati.replace("6", "").length();
            int count7 = rezultati.length() - rezultati.replace("7", "").length();
            int count8 = rezultati.length() - rezultati.replace("8", "").length();

            /////////////////////////////////////////////////////////////////////////////////
            ////////////////////IZRAČUN POSTOTKA////////////////////////////////////////////
            float postotak1 = (float) count1 / (float) rezultati.length() * 100;
            float postotak2 = (float) count2 / (float) rezultati.length() * 100;
            float postotak3 = (float) count3 / (float) rezultati.length() * 100;
            float postotak4 = (float) count4 / (float) rezultati.length() * 100;
            float postotak5 = (float) count5 / (float) rezultati.length() * 100;
            float postotak6 = (float) count6 / (float) rezultati.length() * 100;
            float postotak7 = (float) count7 / (float) rezultati.length() * 100;
            float postotak8 = (float) count8 / (float) rezultati.length() * 100;


            pitanje.setText((DataStorage.ankete[position].pitanje).toString());
            final String q = (DataStorage.ankete[position].pitanje).toString();


            final RadioGroup grp = (RadioGroup) findViewById(R.id.radioGroup1);

            ///////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////RADIO BUTTON////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////

            if (!(DataStorage.ankete[position].opcija1).isEmpty())
                rda.setText((DataStorage.ankete[position].opcija1).toString());
            else
                rda.setVisibility(View.INVISIBLE);

            if (!(DataStorage.ankete[position].opcija2).isEmpty())
                rdb.setText((DataStorage.ankete[position].opcija2).toString());
            else
                rdb.setVisibility(View.INVISIBLE);

            if (!(DataStorage.ankete[position].opcija3).isEmpty())
                rdc.setText((DataStorage.ankete[position].opcija3).toString());
            else
                rdc.setVisibility(View.INVISIBLE);

            if (!(DataStorage.ankete[position].opcija4).isEmpty())
                rdd.setText((DataStorage.ankete[position].opcija4).toString());
            else
                rdd.setVisibility(View.INVISIBLE);

            if (!(DataStorage.ankete[position].opcija5).isEmpty())
                rde.setText((DataStorage.ankete[position].opcija5).toString());
            else
                rde.setVisibility(View.INVISIBLE);

            if (!(DataStorage.ankete[position].opcija6).isEmpty())
                rdf.setText((DataStorage.ankete[position].opcija5).toString());
            else
                rdf.setVisibility(View.INVISIBLE);

            if (!(DataStorage.ankete[position].opcija7).isEmpty())
                rdg.setText((DataStorage.ankete[position].opcija7).toString());
            else
                rdg.setVisibility(View.INVISIBLE);

            if (!(DataStorage.ankete[position].opcija8).isEmpty())
                rdh.setText((DataStorage.ankete[position].opcija8).toString());
            else
                rdh.setVisibility(View.INVISIBLE);


            //////////////////////////////////////////////////////////////////////////////////
            //////////////////PROVJERA DA LI SU VRIJEDNOSTI PRAZNE//////////////////////
            if (Float.isNaN(postotak1))
                postotak1 = 0;
            if (Float.isNaN(postotak2))
                postotak2 = 0;
            if (Float.isNaN(postotak3))
                postotak3 = 0;
            if (Float.isNaN(postotak4))
                postotak4 = 0;
            if (Float.isNaN(postotak5))
                postotak5 = 0;
            if (Float.isNaN(postotak6))
                postotak6 = 0;
            if (Float.isNaN(postotak7))
                postotak7 = 0;
            if (Float.isNaN(postotak8))
                postotak8 = 0;

            String[] items = new String[8];
            SharedPreferences sharedPreferences = getSharedPreferences("postotci", 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            //editor.putInt("size", items.length);
            editor.putFloat("postotak1", postotak1);
            editor.putFloat("postotak2", postotak2);
            editor.putFloat("postotak3", postotak3);
            editor.putFloat("postotak4", postotak4);
            editor.putFloat("postotak5", postotak5);
            editor.putFloat("postotak6", postotak6);
            editor.putFloat("postotak7", postotak7);
            editor.putFloat("postotak8", postotak8);


            editor.commit();

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////ISPIS REZULTATA//////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Log.d("POSTOTAK",postotak1+"");
            if (!(DataStorage.ankete[position].opcija1).isEmpty())
                rez1.setText((DataStorage.ankete[position].opcija1).toString() + " " + new DecimalFormat("#.##").format(postotak1) + "% " + String.valueOf(count1) + " glasova");
            if (!(DataStorage.ankete[position].opcija2).isEmpty())
                rez2.setText((DataStorage.ankete[position].opcija2).toString() + " " + new DecimalFormat("#.##").format(postotak2) + "% " + String.valueOf(count2) + " glasova");
            if (!(DataStorage.ankete[position].opcija3).isEmpty())
                rez3.setText((DataStorage.ankete[position].opcija3).toString() + " " + new DecimalFormat("#.##").format(postotak3) + "% " + String.valueOf(count3) + " glasova");
            if (!(DataStorage.ankete[position].opcija4).isEmpty())
                rez4.setText((DataStorage.ankete[position].opcija4).toString() + " " + new DecimalFormat("#.##").format(postotak4) + "% " + String.valueOf(count4) + " glasova");
            if (!(DataStorage.ankete[position].opcija5).isEmpty())
                rez5.setText((DataStorage.ankete[position].opcija5).toString() + " " + new DecimalFormat("#.##").format(postotak5) + "% " + String.valueOf(count5) + " glasova");
            if (!(DataStorage.ankete[position].opcija6).isEmpty())
                rez6.setText((DataStorage.ankete[position].opcija6).toString() + " " + new DecimalFormat("#.##").format(postotak6) + "% " + String.valueOf(count6) + " glasova");
            if (!(DataStorage.ankete[position].opcija7).isEmpty())
                rez7.setText((DataStorage.ankete[position].opcija7).toString() + " " + new DecimalFormat("#.##").format(postotak7) + "% " + String.valueOf(count7) + " glasova");
            if (!(DataStorage.ankete[position].opcija8).isEmpty())
                rez8.setText((DataStorage.ankete[position].opcija8).toString() + " " + new DecimalFormat("#.##").format(postotak8) + "% " + String.valueOf(count8) + " glasova");


            ////////////////////////////////////////////////////////////////////////////
            //////////////PROVJERI JE LI VOTE VVEĆ CASTAN/////////////////


            butSave.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    // get selected radio button from radioGroup
                    int selectedId = grp.getCheckedRadioButtonId();
                    // find the radiobutton by returned id

                        RadioButton answer = (RadioButton) findViewById(selectedId);
                        Log.d("answer",answer+"");
                    if (answer != null) {


                        Toast.makeText(getApplicationContext(), "Odabir " + answer.getText(), Toast.LENGTH_SHORT).show();
                        String odgovor = answer.getText().toString();
                        char glas = odgovor.charAt(0);

                        String odg = String.valueOf(glas);

                        SharedPreferences sharedPreferences = getSharedPreferences("odabir", 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        editor.putString("odabir", odg);
                        editor.putString("pitanje", q);
                        editor.commit();

                        new Spremi_odabir().execute();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Nije odabrana nijedna opcija!", Toast.LENGTH_SHORT).show();
                    }

                }






            });
        }

        openChart();


    }

    class Spremi_odabir extends AsyncTask<String[], String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ActiveDetails.this);
            pDialog.setMessage("Spremam odabir...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String[]... args) {

            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM. HH:mm");
            String currentDateandTime = sdf.format(new Date());

            SharedPreferences sharedPreferences1 = getSharedPreferences("odabir", 0);

            String odabir = sharedPreferences1.getString("odabir", DEFAULT);
            String pitanje = sharedPreferences1.getString("pitanje", DEFAULT);

            SharedPreferences sharedPreferences2 = getSharedPreferences("login_podaci", 0);
            String voter = sharedPreferences2.getString("name", DEFAULT);
            Log.d("voter", voter);
            Log.d("vrijeme", currentDateandTime + pitanje + odabir);
            List<NameValuePair> params = new ArrayList<NameValuePair>();


            params.add(new BasicNameValuePair(TAG_DATUM, currentDateandTime));

            params.add(new BasicNameValuePair(TAG_PITANJE, pitanje));
            params.add(new BasicNameValuePair(TAG_ODABIR, odabir));
            params.add(new BasicNameValuePair(TAG_VOTER, voter));

            JSONObject json = jsonParser.makeHttpRequest(url,
                    "POST", params);

            // check json success tag
            try {
                Log.d("try", "Pokušaj pristupa bazi");
                int success = json.getInt(TAG_SUCCESS);
                Log.d("resp", json.getString(TAG_SUCCESS));
                SharedPreferences sharedPreferences = getSharedPreferences("success_msg", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                ///////////////////////////////////////////////////////////////////
                ////////////////PROVJERA JE LI VEC CASTAN VOTE////////////////////
                //////////////////////////////////////////////////////////////
                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    Log.d("response", "uspjeh POST-A");


                    editor.putInt("success", success);
                    editor.commit();

                    finish();
                } else {
                    Log.d("GRESKA", "greska");

                    editor.putInt("success", success);
                    editor.commit();
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("Debug", e.toString());

            }

            return null;

        }


        protected void onPostExecute(String file_url) {
            SharedPreferences sharedPreferences = getSharedPreferences("success_msg", 0);
            int success = sharedPreferences.getInt("success", 0);
            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Odgovor pohranjen", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Već ste glasali!", Toast.LENGTH_SHORT).show();
            }

            Intent intent = new Intent(getApplicationContext(), Izbornik.class);
            startActivity(intent);
            pDialog.dismiss();

        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_active_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void openChart() {

        ////PRVO TREBA UCI U MAINACTIVITY DA SE OSTVARI URL KONKECIJA

        //int[] x = {0, 1, 2};
        Bundle extras = getIntent().getExtras();
        if (extras != null) {





            //String[] x= {"1","2","3"};
            String value = extras.getString("pozicija");
            Integer position = Integer.valueOf(value);
            String pitanje = DataStorage.ankete[position].pitanje;
            String opcija1 = DataStorage.ankete[position].opcija1;
            String opcija2 = DataStorage.ankete[position].opcija2;
            String opcija3 = DataStorage.ankete[position].opcija3;
            String opcija4 = DataStorage.ankete[position].opcija4;
            String opcija5 = DataStorage.ankete[position].opcija5;
            String opcija6 = DataStorage.ankete[position].opcija6;
            String opcija7 = DataStorage.ankete[position].opcija7;
            String opcija8 = DataStorage.ankete[position].opcija8;


            XYSeries incomeSeries1 = new XYSeries(opcija1);
            XYSeries incomeSeries2 = new XYSeries(opcija2);
            XYSeries incomeSeries3 = new XYSeries(opcija3);
            XYSeries incomeSeries4 = new XYSeries(opcija4);
            XYSeries incomeSeries5 = new XYSeries(opcija5);
            XYSeries incomeSeries6 = new XYSeries(opcija6);
            XYSeries incomeSeries7 = new XYSeries(opcija7);
            XYSeries incomeSeries8 = new XYSeries(opcija8);
            // Creating an XYSeries for Expense
           // XYSeries expenseSeries = new XYSeries("Humidity");

            // Adding data to Income and Expense Series

            SharedPreferences sharedPreferences2 = getSharedPreferences("postotci", 0);
            Float postotak1 = sharedPreferences2.getFloat("postotak1", 0);
            Float postotak2 = sharedPreferences2.getFloat("postotak2", 0);
            Float postotak3 = sharedPreferences2.getFloat("postotak3", 0);
            Float postotak4 = sharedPreferences2.getFloat("postotak4", 0);
            Float postotak5 = sharedPreferences2.getFloat("postotak5", 0);
            Float postotak6 = sharedPreferences2.getFloat("postotak6", 0);
            Float postotak7 = sharedPreferences2.getFloat("postotak7", 0);
            Float postotak8 = sharedPreferences2.getFloat("postotak8", 0);

            incomeSeries1.add(1, postotak1);
            incomeSeries2.add(2, postotak2);
            incomeSeries3.add(3, postotak3);
            incomeSeries4.add(4, postotak4);
            incomeSeries5.add(5, postotak5);
            incomeSeries6.add(6, postotak6);
            incomeSeries7.add(7, postotak7);
            incomeSeries8.add(8, postotak8);



// Creating a dataset to hold each series
            // set the start angle for the first slice in the pie chart
            mRenderer.setStartAngle(180);
// display values on the pie slices
            mRenderer.setDisplayValues(true);
            XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();

// Adding Income Series to the dataset
            //dataset.addSeries(incomeSeries);

            dataset.addSeries(incomeSeries1);
            dataset.addSeries(incomeSeries2);
            dataset.addSeries(incomeSeries3);
            dataset.addSeries(incomeSeries4);
            dataset.addSeries(incomeSeries5);
            dataset.addSeries(incomeSeries6);
            dataset.addSeries(incomeSeries7);
            dataset.addSeries(incomeSeries8);


// Creating XYSeriesRenderer to customize incomeSeries
            XYSeriesRenderer incomeRenderer1 = new XYSeriesRenderer();
            incomeRenderer1.setColor(Color.GREEN); //color of the graph set to cyan //TEMP
            incomeRenderer1.setFillPoints(false);
            incomeRenderer1.setLineWidth(2f);
            incomeRenderer1.setDisplayChartValues(true);
//setting chart value distance
            incomeRenderer1.setDisplayChartValuesDistance(10);
//setting line graph point style to circle
            incomeRenderer1.setPointStyle(PointStyle.CIRCLE);
//setting stroke of the line chart to solid
            incomeRenderer1.setStroke(BasicStroke.SOLID);
            incomeRenderer1.setChartValuesFormat(NumberFormat.getNumberInstance());
            incomeRenderer1.setAnnotationsTextSize(35);


            // Creating XYSeriesRenderer to customize incomeSeries
            XYSeriesRenderer incomeRenderer2 = new XYSeriesRenderer();
            incomeRenderer2.setColor(Color.CYAN); //color of the graph set to cyan //TEMP
            incomeRenderer2.setFillPoints(false);
            incomeRenderer2.setLineWidth(2f);
            incomeRenderer2.setDisplayChartValues(true);
//setting chart value distance
            incomeRenderer2.setDisplayChartValuesDistance(10);
//setting line graph point style to circle
            incomeRenderer2.setPointStyle(PointStyle.SQUARE);
//setting stroke of the line chart to solid
            incomeRenderer2.setStroke(BasicStroke.SOLID);


            // Creating XYSeriesRenderer to customize incomeSeries
            XYSeriesRenderer incomeRenderer3 = new XYSeriesRenderer();
            incomeRenderer3.setColor(Color.RED); //color of the graph set to cyan //TEMP
            incomeRenderer3.setFillPoints(true);
            incomeRenderer3.setLineWidth(2f);
            incomeRenderer3.setDisplayChartValues(true);
//setting chart value distance
            incomeRenderer3.setDisplayChartValuesDistance(10);
//setting line graph point style to circle
            incomeRenderer3.setPointStyle(PointStyle.DIAMOND);
//setting stroke of the line chart to solid
            incomeRenderer3.setStroke(BasicStroke.SOLID);



            // Creating XYSeriesRenderer to customize incomeSeries
            XYSeriesRenderer incomeRenderer4 = new XYSeriesRenderer();
            incomeRenderer4.setColor(Color.BLUE); //color of the graph set to cyan //TEMP
            incomeRenderer4.setFillPoints(false);
            incomeRenderer4.setLineWidth(2f);
            incomeRenderer4.setDisplayChartValues(true);
//setting chart value distance
            incomeRenderer4.setDisplayChartValuesDistance(5);
//setting line graph point style to circle
            incomeRenderer4.setPointStyle(PointStyle.CIRCLE);
//setting stroke of the line chart to solid
            incomeRenderer4.setStroke(BasicStroke.SOLID);



            // Creating XYSeriesRenderer to customize incomeSeries
            XYSeriesRenderer incomeRenderer5 = new XYSeriesRenderer();
            incomeRenderer5.setColor(Color.GREEN); //color of the graph set to cyan //TEMP
            incomeRenderer5.setFillPoints(true);
            incomeRenderer5.setLineWidth(2f);
            incomeRenderer5.setDisplayChartValues(true);
//setting chart value distance
            incomeRenderer5.setDisplayChartValuesDistance(5);
//setting line graph point style to circle
            incomeRenderer5.setPointStyle(PointStyle.CIRCLE);
//setting stroke of the line chart to solid
            incomeRenderer5.setStroke(BasicStroke.SOLID);



            // Creating XYSeriesRenderer to customize incomeSeries
            XYSeriesRenderer incomeRenderer6 = new XYSeriesRenderer();
            incomeRenderer6.setColor(Color.CYAN); //color of the graph set to cyan //TEMP
            incomeRenderer6.setFillPoints(true);
            incomeRenderer6.setLineWidth(2f);
            incomeRenderer6.setDisplayChartValues(true);
//setting chart value distance
            incomeRenderer6.setDisplayChartValuesDistance(5);
//setting line graph point style to circle
            incomeRenderer6.setPointStyle(PointStyle.CIRCLE);
//setting stroke of the line chart to solid
            incomeRenderer6.setStroke(BasicStroke.SOLID);


            // Creating XYSeriesRenderer to customize incomeSeries
            XYSeriesRenderer incomeRenderer7 = new XYSeriesRenderer();
            incomeRenderer7.setColor(Color.RED); //color of the graph set to cyan //TEMP
            incomeRenderer7.setFillPoints(true);
            incomeRenderer7.setLineWidth(2f);
            incomeRenderer7.setDisplayChartValues(true);
//setting chart value distance
            incomeRenderer7.setDisplayChartValuesDistance(5);
//setting line graph point style to circle
            incomeRenderer7.setPointStyle(PointStyle.CIRCLE);
//setting stroke of the line chart to solid
            incomeRenderer7.setStroke(BasicStroke.SOLID);



            // Creating XYSeriesRenderer to customize incomeSeries
            XYSeriesRenderer incomeRenderer8 = new XYSeriesRenderer();
            incomeRenderer8.setColor(Color.BLUE); //color of the graph set to cyan //TEMP
            incomeRenderer8.setFillPoints(true);
            incomeRenderer8.setLineWidth(2f);
            incomeRenderer8.setDisplayChartValues(true);
//setting chart value distance
            incomeRenderer8.setDisplayChartValuesDistance(5);
//setting line graph point style to circle
            incomeRenderer8.setPointStyle(PointStyle.CIRCLE);
//setting stroke of the line chart to solid
            incomeRenderer8.setStroke(BasicStroke.SOLID);




// Creating a XYMultipleSeriesRenderer to customize the whole chart
            XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
            multiRenderer.setXLabels(0);
            multiRenderer.setChartTitle(pitanje);
            multiRenderer.setXTitle("");
            multiRenderer.setYTitle("");

/***
 * Customizing graphs
 */
//setting text size of the title
            multiRenderer.setChartTitleTextSize(48);  //////NASLOV
//setting text size of the axis title
            multiRenderer.setAxisTitleTextSize(48);
//setting text size of the graph lable
            multiRenderer.setLabelsTextSize(30);
//setting zoom buttons visiblity
            multiRenderer.setZoomButtonsVisible(true);
//setting pan enablity which uses graph to move on both axis
            multiRenderer.setPanEnabled(false, false);
//setting click false on graph
            multiRenderer.setClickEnabled(false);
//setting zoom to false on both axis
            multiRenderer.setZoomEnabled(false, false);
//setting lines to display on y axis
            multiRenderer.setShowGridY(true);
//setting lines to display on x axis
            multiRenderer.setShowGridX(true);
//setting legend to fit the screen size
            multiRenderer.setFitLegend(true);
//setting displaying line on grid
            multiRenderer.setShowGrid(true);
//setting zoom to false
            multiRenderer.setZoomEnabled(false);
//setting external zoom functions to false
            multiRenderer.setExternalZoomEnabled(false);
//setting displaying lines on graph to be formatted(like using graphics)
            multiRenderer.setAntialiasing(true);
//setting to in scroll to false
            multiRenderer.setInScroll(false);
//setting to set legend height of the graph
            //  multiRenderer.setLegendHeight(5);
            multiRenderer.setLegendTextSize(36);                 //LEGENDA

//setting x axis label align
            multiRenderer.setXLabelsAlign(Paint.Align.RIGHT);
//setting y axis label to align
            multiRenderer.setYLabelsAlign(Paint.Align.CENTER);
//setting text style
            multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL);
//setting no of values to display in y axis
            multiRenderer.setYLabels(10);
// setting y axis max value, Since i'm using static values inside the graph so i'm setting y max value to 4000.
// if you use dynamic values then get the max y value and set here
            multiRenderer.setYAxisMin(0);
            multiRenderer.setYAxisMax(100);
//setting used to move the graph on xaxiz to .5 to the right
            multiRenderer.setXAxisMin(-1.5);
//setting used to move the graph on xaxiz to .5 to the right
            multiRenderer.setXAxisMax(11);
//setting bar size or space between two bars
//multiRenderer.setBarSpacing(0.1);
            multiRenderer.setBarWidth(35);
            multiRenderer.setDisplayValues(false);
//Setting background color of the graph to transparent
            multiRenderer.setBackgroundColor(Color.BLACK);
//Setting margin color of the graph to transparent
            multiRenderer.setMarginsColor(getResources().getColor(R.color.background_material_dark));
            multiRenderer.setApplyBackgroundColor(true);
            multiRenderer.setScale(2f);
            multiRenderer.setAxisTitleTextSize(40);
            multiRenderer.setShowTickMarks(false);

//setting x axis point size
            multiRenderer.setPointSize(6f);
//setting the margin size for the graph in the order top, left, bottom, right
            multiRenderer.setMargins(new int[]{77, 30, 130, 30});                //MARGINE

         //   for (int i = 0; i < x.length; i++) {
         //       multiRenderer.addXTextLabel(i, mMonth[i]);
         //   }

// Adding incomeRenderer and expenseRenderer to multipleRenderer
// Note: The order of adding dataseries to dataset and renderers to multipleRenderer
// should be same
            multiRenderer.addSeriesRenderer(incomeRenderer1);
            multiRenderer.addSeriesRenderer(incomeRenderer2);
            multiRenderer.addSeriesRenderer(incomeRenderer3);
            multiRenderer.addSeriesRenderer(incomeRenderer4);
            multiRenderer.addSeriesRenderer(incomeRenderer5);
            multiRenderer.addSeriesRenderer(incomeRenderer6);
            multiRenderer.addSeriesRenderer(incomeRenderer7);
            multiRenderer.addSeriesRenderer(incomeRenderer8);
           // multiRenderer.addSeriesRenderer(expenseRenderer);


//this part is used to display graph on the xml
            LinearLayout chartContainer = (LinearLayout) findViewById(R.id.chart);

            mSeries.add("Series " + (mSeries.getItemCount() + 1), 3);
            SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
            renderer.setColor(COLORS[(mSeries.getItemCount() - 1) % COLORS.length]);
            mRenderer.addSeriesRenderer(renderer);
            // mChart.repaint();

//remove any views before u paint the chart
            chartContainer.removeAllViews();
//drawing bar chart
            mChart = ChartFactory.getBarChartView(ActiveDetails.this, dataset, multiRenderer, BarChart.Type.DEFAULT);
//adding the view to the linearlayout
            chartContainer.addView(mChart);

        }
    }
}
