package com.example.neno.chatapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by neno on 6.3.2016..
 */
public class AdapterActive extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;


    public AdapterActive(Context context)
    {

        mContext = context;
        mInflater=(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        return DataStorage.ankete.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView = mInflater.inflate(R.layout.item_active,parent,false);


        TextView Datum = (TextView) convertView.findViewById(R.id.datum);
        TextView Autor = (TextView) convertView.findViewById(R.id.autor);
        TextView Pitanje = (TextView) convertView.findViewById(R.id.pitanje);

        SharedPreferences sharedPreferences = mContext.getSharedPreferences("login_podaci", 0);


        //Korisnik.setText(name + ": ");
        Datum.setText((DataStorage.ankete[position].datum).toString() + " ");
        Pitanje.setText((DataStorage.ankete[position].pitanje).toString()+" ");
        Autor.setText("Autor: " + (DataStorage.ankete[position].autor).toString());


        return convertView;
    }
}
