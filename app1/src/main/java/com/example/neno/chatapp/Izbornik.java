package com.example.neno.chatapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class Izbornik extends ActionBarActivity {

    Button novo_glasanje;
    Button aktualna_glasanja;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izbornik);
        Button aktualna_glasanja = (Button) findViewById(R.id.btn_current);
        Button novo_glasanje = (Button) findViewById(R.id.btn_novo);
        Button chat = (Button) findViewById(R.id.btn_chat);
        Button odjavi = (Button) findViewById(R.id.btn_odjavi);



        novo_glasanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),CreatePoll.class);
                startActivity(intent);
            }
        });

        aktualna_glasanja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),Active.class);
                startActivity(intent);
            }
        });

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });


        odjavi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                SharedPreferences sharedPreferences = getSharedPreferences("login_podaci", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();  //SPREMI PODATKE U OBJEKT KLASE SharedPreferences
                editor.clear();
                editor.commit();


                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
            }

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate() == true) {
            displayUserDetails();
        }
    }

    private boolean authenticate() {
        if (LoggedIn() == false) {
            Intent intent = new Intent(getApplicationContext(), Login.class);
            startActivity(intent);
            return false;
        }
        return true;
    }

    private boolean LoggedIn()
    {
        SharedPreferences myPrefs = this.getSharedPreferences("login_podaci", Context.MODE_PRIVATE);
        String username = myPrefs.getString("name",null);
        String password = myPrefs.getString("password",null);
        if(username != null && password != null)
            return true;
        else
            return false;


    }

    private void displayUserDetails() {
        Log.d("Dobrodosao", "user");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_izbornik, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
